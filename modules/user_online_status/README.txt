CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The User Online Status module display live status (i.e Online/Offline) of the
user to their respective account/profile page.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/user_online_status

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/user_online_status

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.

CONFIGURATION
-------------

No configuration is needed.

MAINTAINERS
-----------

Current maintainers:
 * Norman Kämper-Leymann (leymannx) - https://www.drupal.org/u/leymannx
 * Arthur Lorenz (arthur_lorenz) - https://www.drupal.org/u/arthur_lorenz
