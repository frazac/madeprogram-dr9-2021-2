<?php

/**
 * @file
 * Post update hooks for User Online Status module.
 */

/**
 * Clear caches due to updated file names.
 */
function user_online_status_post_update_cache_rebuild_8100() {
  // Empty post-update hook.
}

/**
 * Clear caches due to newly added service.
 */
function user_online_status_post_update_cache_rebuild_8101() {
  // Empty post-update hook.
}

/**
 * Clear caches due to refactored Views field.
 */
function user_online_status_post_update_cache_rebuild_8102() {
  // Empty post-update hook.
}

/**
 * Clear caches due to newly added route and controller.
 */
function user_online_status_post_update_cache_rebuild_8103() {
  // Empty post-update hook.
}
