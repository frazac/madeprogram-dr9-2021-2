//(function ($, Drupal) {
//    Drupal.behaviors.mainJS = {
//        attach: function attach(context) {

(function ($, Drupal) {
    Drupal.behaviors.principale = {
        attach: function (context, settings) {

// variabili
            var debugMenu       = false;
            var debugMenuMobile = false;
            
            var $headerRegion               = $( "header .region-header" );
            var $headerRegionLogoWrapper    = $( "header .region-header .site-logo-wrapper" );
            var $menuLiv0Menu               = $( ".menuPrincipale" );
            var menuLiv0MenuClass           = "menuPrincipale";
            var $menuLiv0MenuContainer      = $( ".menuPrincipale .menuViewLivello0 > .view-content" );
            var $menuLiv0Voce               = $( ".menuPrincipale .menuLiv0Voce" );
            var $menuOrizzontale            = $( ".menuPrincipale .menuOrizzontale" );
            var $menuSottomenu              = $( ".menuPrincipale .menuLiv0Voce > .sottoMenu" );
            var $activeMenu                 = 0; // preparo variabile per dom
            var activeMenu                  = ''; // preparo variabile stringa per selettori
            var $menuBackground             = $( "#block-menu1dropdownbackground" ); //trigger hide menu
            var menuBackgroundY             = ($menuBackground.offset()).top;
            var $menuAsideSx                = $( "#block-menu-2-asidesx" ); //.block-views-blockmenu-2-aside-sx-block-1
            //var $menuAsideDx                = $( "#block-menu-dd-dx-ita" ); // fixme italiano
            var $menuAsideDx                = $( ".menu_dd_dx" );
            var myHeightArray               = [];
            var viewportHeight              = $(window).height();
            var activeClass                 = "active";
            var activeClassNoTransition     = "active noTransition";
            //var headerRegionPos             = $headerRegion.offset().top;
            var headerRegionPos             = 10;
            var $bannerCorsiView            = $( ".view-banner-corsi" );
            var $searchWrapper              = $( "#block-fluffiness-search" );
            var $searchLente                = $( "#block-fluffiness-search .lente" );
            var $searchCampo                = $( "#block-fluffiness-search .campo" );
            var $carosello                  = $( ".view-carosello" );
            var caroselloCorso              = "caroselloCorso";
            var caroselloHomepage           = "caroselloHomepage";
            var $caroselloDots              = $( "ul.slick-dots" );
            var $caroselloFrecce            = $( ".slick nav.slick__arrow" );
            var $caroselloBlocchetto        = $( ".blocchetto" ); //relativo, sono più d'uno e nascosti
            var $caroselloSlide             = $( ".view-carosello .slick-slide" ); //relativo, sono più d'uno e nascosti
            var $menuMobile                 = $( ".menuPrincipaleMobile" );
            var $bannerCorsiVoce            = $bannerCorsiView.find( ".banner" );
            var myPrevImg                   = "";
            var $iFrameTBW                  = $( ".iframeTo600" );
            var $iFrameTBW2                 = $( ".iframeTo100p" );
            var $hamburger                  = $( ".hamburger" );
            var $blockHamburger             = $( "#block-menu1hamburger" );
            var $primeVociMenuFooter        = $( ".menuFooter .menuViewLivello0 > .view-content > .views-row > .menuLiv0Voce > .voce a" );
            var $mostraIndice               = $( ".stile-Mostra-indice-menu-di-pagina" );
            var $root                       = $('html, body');
// fine delle variabili
// tutte le function
            function searchActive() {
                if ($("body").hasClass("menuActive")) {
                    //menu attivo
                    setTimeout(function(){
                        $searchCampo.addClass( "active" );
                        $searchLente.addClass( "active" );
                        $searchWrapper.addClass( "active" );
                        $("body").addClass("menuSearchActive")
                    }, 250);
                } else {
                    $searchCampo.addClass( "active" );
                    $searchLente.addClass( "active" );
                    $searchWrapper.addClass( "active" );
                    $("body").addClass("menuSearchActive")
                };
            }
            function searchHide() {
                $searchCampo.removeClass( "active" );
                $searchLente.removeClass( "active" );
                $searchWrapper.removeClass( "active" );
                $("body").removeClass("menuSearchActive")
            }
            function menuActive($submenuActive,resetSubmenu,myClass,withBackground) {
                if ( $("body").hasClass("menuSearchActive") ) {
                    searchHide();
                    setTimeout(function(){
                        //$( '#block-menu-dd-dx-ita' ).addClass( myClass ); // fixme ita
                        $menuAsideDx.addClass( myClass );
                        $( '#block-menu-2-asidesx' ).addClass( myClass );
                        $menuBackground.addClass( myClass );
                        $( 'body' ).addClass( "menuActive" );
                        if (resetSubmenu) {
                            $menuLiv0Voce.removeClass( myClass );
                        }
                        $submenuActive.addClass( myClass );
                        if (withBackground) {
                            //con---sole.log(activeMenu);
                            //con---sole.log(myHeightArray[activeMenu]);
                            activeMenu = $submenuActive.attr("class").split(/\s/g)[1]; //.replace(" ",".")
                            $menuBackground.css("height",myHeightArray[activeMenu]);
                            //con---sole.log("myHeightArray[" + activeMenu + "]: " + myHeightArray[activeMenu]);
                        }
                    }, 250);
                } else {
                    searchHide();
                    //$( '#block-menu-dd-dx-ita' ).addClass( myClass ); //fixme ita
                    $menuAsideDx.addClass( myClass );
                    $( '#block-menu-2-asidesx' ).addClass( myClass );
                    $menuBackground.addClass( myClass );
                    $( 'body' ).addClass( "menuActive" );
                    if (resetSubmenu) {
                        $menuLiv0Voce.removeClass( myClass );
                    }
                    $submenuActive.addClass( myClass );
                    if (withBackground) {
                        //con---sole.log(activeMenu);
                        //con---sole.log(myHeightArray[activeMenu]);
                        activeMenu = $submenuActive.attr("class").split(/\s/g)[1]; //.replace(" ",".")
                        $menuBackground.css("height",myHeightArray[activeMenu]);
                        //con---sole.log("myHeightArray[" + activeMenu + "]: " + myHeightArray[activeMenu]);
                    }
                }
                
            }
            function menuHide(myClass) {
                //con---sole.log("vai con menuhide: " + myClass);
                if (myClass == activeClassNoTransition) {
                //con---sole.log("vai con menuhide activeClassNoTransition: " + myClass);
                    myRemovingClass = myClass.split(/\s/g);
                //con---sole.log( "myRemovingClass[0]" + myRemovingClass[0] );
                //con---sole.log( "myRemovingClass[1]" + myRemovingClass[1] );
                    $menuAsideDx.removeClass( myRemovingClass[0] );
                    $menuAsideSx.removeClass( myRemovingClass[0] );
                    $menuBackground.removeClass( myRemovingClass[0] );
                    $menuBackground.css("height","");
                    $menuLiv0Voce.removeClass( myRemovingClass[0] );
                    $( 'body' ).removeClass( "menuActive" );
                    setTimeout(function(){
                        $menuAsideDx.removeClass( myRemovingClass[1] );
                        $menuAsideSx.removeClass( myRemovingClass[1] );
                        $menuBackground.removeClass( myRemovingClass[1] );
                        $menuLiv0Voce.removeClass( myRemovingClass[1] );
                    }, 1000);
                } else {
                //con---sole.log("*** else *** "+myClass+" *=*");
                    $menuAsideSx.removeClass( myClass );
                    $menuAsideDx.removeClass( myClass );
                    $menuBackground.removeClass( myClass );
                    $menuBackground.css("height","");
                    $menuLiv0Voce.removeClass( myClass );
                    $( 'body' ).removeClass( "menuActive" );
                }
            }
            function menuOrizzontalePosiziona () {
                //con---sole.log("hei");            
                if ($menuAsideSx.length && $menuAsideSx.css('display') != 'none') {
                    menuAsideSx_RIGHT = $menuAsideSx.offset().left + $menuAsideSx.outerWidth();
                }
                else {
                    menuAsideSx_RIGHT = 0;
                };
                if ($menuAsideDx.length && $menuAsideDx.css('display') != 'none') {
                    menuAsideDx_LEFT = $menuAsideDx.offset().left;
                }
                else {
                    menuAsideDx_LEFT = $headerRegion.width();
                };
                //con---sole.log("menuAsideSx_RIGHT: " + menuAsideSx_RIGHT);
                //con---sole.log("menuAsideDx_LEFT: " + menuAsideDx_LEFT);
                $menuOrizzontale.each(function(index, value) {
                    $menuOrizzontaleSottomenu = $(this).find( ".menuViewLivello2 > .view-content" );
                    $menuOrizzontaleSottomenu = $(this).children( ".sottoMenu" );
                    //$menuOrizzontaleSottomenu.css("border-left","1px solid blue");
                    myLarghezza = $menuOrizzontaleSottomenu.width();
                    myPosizione = $menuOrizzontaleSottomenu.offset().left;
                    targetPosizione = ( ( menuAsideDx_LEFT - menuAsideSx_RIGHT) / 2 ) + menuAsideSx_RIGHT - ( myLarghezza / 2);
                    //con---sole.log("$menuOrizzontale width:" + myLarghezza);
                    //con---sole.log("myPosizione:" + myPosizione);
                    //con---sole.log("targetPosizione:" + targetPosizione);
                    //$menuOrizzontale.children( ".sottoMenu" ).css("left","-420px");
                    //$(this).children(".sottomenu").offset( { left: targetPosizione } );
                    //$(this).children( ".sottoMenu" ).css("overflow","visible");
                    $menuOrizzontaleSottomenu.offset( { left: targetPosizione } );
                });
            }
            // inizializza il menu al caricamento della pagina per calcolare le altezze giuste
            function menuInit () {
                //con---sole.log("function menuInit");
                $menuSottomenu.addClass("invisibile");
                $menuAsideSx.addClass("invisibile");
                $menuAsideDx.addClass("invisibile");
                $menuBackground.addClass("invisibile");
                $menuLiv0Voce.each(function(index, value) {
                    myClasses = $(this).attr('class').split(" ");
                    myClass = "." + myClasses[1];
                    //con---sole.log(myClass);
                    menuActive($(myClass),false,activeClassNoTransition,false);
                    menuCalcolaHInit($(this));
                    menuHide(activeClassNoTransition);
                });
                $menuSottomenu.removeClass("invisibile");
                $menuAsideSx.removeClass("invisibile");
                $menuAsideDx.removeClass("invisibile");
                $menuBackground.removeClass("invisibile");
            }
            function menuCalcolaHInit($activeMenu) { // calcola altezza del background... in background
                //con---sole.log("function menuCalcolaHInit");
                var hMenuAttivo = $activeMenu.find(".menuViewLivello2").height();
                var hMenuAsideDx = $menuAsideDx.find(".field--name-body").height();
                if ( $menuAsideSx.length ) {
                    var hMenuAsideSx = $menuAsideSx.find(".view").height();
                } else {
                    var hMenuAsideSx = 0;                    
                };
                //con---sole.log("le tre altezze");
                //con---sole.log(hMenuAttivo);
                //con---sole.log(hMenuAsideSx);
                //con---sole.log(hMenuAsideDx);
                var myArray = [hMenuAttivo,hMenuAsideSx,hMenuAsideDx];
                var maxHeight = (Math.max.apply(Math,myArray)) + 20;            
                activeMenu = $activeMenu.attr("class").split(/\s/g)[1]; // /\s/g = " " ma funziona anche con spazi creati da jquery
                //con---sole.log("activeMenu: " + activeMenu);
                //con---sole.log("maxHeight: " + maxHeight);
                myHeightArray[activeMenu] = maxHeight;
                //con---sole.log("myHeightArray[" + activeMenu + "]: " + myHeightArray[activeMenu]);
                //con---sole.log("maxHeight[activeMenu] : activeMenu / myHeightArray[activeMenu]");
                //con---sole.log(activeMenu);
                //con---sole.log(myHeightArray[activeMenu]);
                //viewportHeight = $(window).height();
                //menuBackgroundY = ($menuBackground.offset()).top;
                //if ( maxHeight + menuBackgroundY >= viewportHeight ) {
                //    hamburgerMenuActive();
                //}
            }
            //calcola altezza blocchetto
            function caroselloInitControl($myElement) {
                //caroselloCorso
                //caroselloHomepage
                if ($carosello.hasClass(caroselloHomepage)) {
                    //myElement.find($caroselloBlocchetto).css("border","1px solid red"); // fixme TypeError: myElement is undefined
                    var myBlocchetto = $myElement.find($caroselloBlocchetto);
                    var posizione = myBlocchetto.position().top - 11 - 20;
                    //$caroselloDots.css("margin-top",posizione);
                    $( "ul.slick-dots" ).css("margin-top",posizione); // $caroselloDots non funziona
                    $caroselloFrecce.css("margin-top",posizione);
                    //con---sole.log(posizione);
                } else if ($carosello.hasClass(caroselloCorso)) {
                    $caroselloFrecce.css("margin-top","10px");
                    $caroselloFrecce.css("top","auto");
                    $caroselloFrecce.css("bottom","20px");
                    //$caroselloDots.css("margin-top","10px");
                    //$caroselloDots.css("top","auto");
                    //$caroselloDots.css("bottom","20px");
                    $( "ul.slick-dots" ).css("margin-top","10px");
                    $( "ul.slick-dots" ).css("top","auto");
                    $( "ul.slick-dots" ).css("bottom","20px");
                }
            };
            function hamburgerMenuActive() {
                $menuMobile.toggleClass( "active" );
                $( "body" ).toggleClass( "hamburgerMenuActive" );
            }
            function hamburgerMenuHide() {
                $menuMobile.removeClass( "active" );
                $( "body" ).removeClass( "hamburgerMenuActive" );
            }
            function menuIndiceLoading() {
                var myVar = '<div class="menuIndice"><div class="wrapper"><ul id="tocList"><li>Caricando l\'indice...</li></ul></div></div>';
                return myVar;
            }
            function menuIndiceTOC() {
                $("#tocList").empty();
                var prevH2Item = null;
                var prevH2List = null;
                var index = 0;
                $("article h2").each(function() {                               // $("h2, h3").each(function()
                    if ( !$(this).hasClass( "visually-hidden" ) ) {     //fixme aggiungere una eccezione per menu strumenti quando loggato dentro
                        //insert an anchor to jump to, from the TOC link.
                        var anchor = "<a class='segnaposto-anchor' name='" + index + "'></a>";
                        //console.log( $(this).prev().attr("class") );
                        var previous = $(this).prev().attr("class");
                        //var previousCheck = $(this).prev().attr("class").indexOf("wp-block-image");
                        if ( typeof previous !== 'undefined' ) {
                            console.log( previous );
                            $(this).prev().before(anchor);
                        } else {
                            $(this).before(anchor);
                        }
                        //if ( previousCheck > -1 ) {
                        //    console.log("previous == wp-block-image");
                        //    $(this).prev().before(anchor);
                        //} else {
                        //    console.log("previous <> wp-block-image");
                        //    $(this).before(anchor);
                        //}
                        //$(this).before(anchor);
                        var li     = "<li><a href='#" + index + "'>" + $(this).text() + "</a></li>";
                        if( $(this).is("h2") ){
                            prevH2List = $("<ul></ul>");
                            prevH2Item = $(li);
                            prevH2Item.append(prevH2List);
                            prevH2Item.appendTo("#tocList");
                        } else {
                            prevH2List.append(li);
                        }
                    }
                    index++;
                });
            };
// fine delle function
// inizio codice per azioni            
            $("body").once("script-generici-once").each(function () {
                // frecce carosello spariscono, per dare tempo allo script di dare giusta forma
                if ( $carosello.length ) {
                    $caroselloFrecce.css("visibility","hidden");
                    //$caroselloDots.css("visibility","hidden");
                    $( "ul.slick-dots" ).css("visibility","hidden");
                }
                // inizializza il menu per regolare le altezze
                menuInit();
                if ($menuOrizzontale.length) {
                    menuOrizzontalePosiziona();
                };
                // debug menu
                if (debugMenu) {
                    setTimeout(function(){
                        menuActive($(".menuLiv0VoceTid3"),false,activeClass,true);
                    }, 2000);
                };
                if (debugMenuMobile) {
                    $menuMobile.addClass( "active" );
                }
                headerRegionPos = $headerRegion.offset().top;
                if ($("body").hasClass("role-anonymous")) {
                    //anche il css nasconde ma qui togliamo il pezzo
                    $(".soloEditor").remove();
                    $(".statoNonPubblicato").remove();
                };
                // se c'è view titolo nascondi il vero titolo in favore della view (pagine corsi)
                var $viewTitle = $( ".view-titolo" );
                var $viewTitleReplace = $( "#block-fluffiness-page-title" );
                if ( $viewTitle.length ) {
                    $viewTitleReplace.hide();
                }
                // se c'è lo stile "stile-Titolo-sopra-carosello" sposta verso il basso il titolo (view-titolo) al punto giusto
                if ( $viewTitle.length && $("body").hasClass("stile-Titolo-sopra-carosello") ) {
                    var $myBoxWTitle = $viewTitle.parent(".views-element-container").parent(".field--name-body");
                    var hCarosello = ($myBoxWTitle.next(".field--name-body").find("img").height()) / 2;
                    var hTitolo = ($myBoxWTitle.height()) / 2;
                    $myBoxWTitle.parent().css("position","relative");
                    $myBoxWTitle.css("position","absolute");
                    $myBoxWTitle.css("width","100%");
                    $myBoxWTitle.css("top",hCarosello-hTitolo);
                }
                // forse obsoleto , il titolo creativo è diventato prima didascalia poi titolo della pagina stilizzato quando c'e' il carosello
                if ( $("body").hasClass("stile-Immagine-di-apertura") ) {
                    var myHeight = $(".field--name-field-immagine").height();
                    //con---sole.log(myHeight);
                    $(".field--name-field-tcreativo").css("top",(-myHeight/2));
                };
                //risolve creazione paragrafo vuoto
                if ( $("body").hasClass("page-node-type-progetto") ) {
                    $pDaTogliere = $( "body .progettiColonne3 .views-element-container" ).prev();
                    var controlloStringa = $pDaTogliere.text().replace(/\s/g,"");
                    if ( !controlloStringa.length ) {
                        $pDaTogliere.hide();
                    }
                }
                //calcolo dimensione iframe youtube (occhio al filetto nero)
                if ( $iFrameTBW.length ) {
                    var myWidth = $iFrameTBW.find("iframe").attr("width");
                    var myHeight = $iFrameTBW.find("iframe").attr("height");
                    //con---sole.log(myWidth+" - "+myHeight);
                    $iFrameTBW.find("iframe").attr("width",600);
                    myHeight = Math.round((myHeight*600/myWidth)+0.5);
                    //con---sole.log("600 - "+myHeight);
                    $iFrameTBW.find("iframe").attr("height",myHeight);
                }
                if ( $iFrameTBW2.length ) {
                    var myWidth = $iFrameTBW2.find("iframe").attr("width");
                    var myHeight = $iFrameTBW2.find("iframe").attr("height");
                    //con---sole.log(myWidth+" - "+myHeight);
                    var myBoxW = $iFrameTBW2.width();
                    //con---sole.log(myBoxH);
                    myHeight = Math.round((myHeight*myBoxW/myWidth)+0.5);
                    //con---sole.log(myBoxW+" - "+myHeight);
                    $iFrameTBW2.find("iframe").attr("width","100%");
                    $iFrameTBW2.find("iframe").attr("height",myHeight);
                }
                // indice di pagina lunga, menu secondario
                var menuIndiceHtml = menuIndiceLoading();
                if ($('body').hasClass("stile-Mostra-indice-menu-di-pagina")) {
                    $( menuIndiceHtml ).insertAfter( $blockHamburger );
                    var hNewHeader = $headerRegion.height() + $(".menuIndice").height();
                    //con---sole.log("hNewHeader = $headerRegion.height() + $(.menuIndice).height() = " + $headerRegion.height() + " + " + $(".menuIndice").height());
                    //$headerRegion.css("height",hNewHeader);
                    menuIndiceTOC();
                }
            }); // fine once su body
            
            // quando tutto è caricato inclusi iframe
            $( window ).on( "load", function() {
                // se c'e' carosello
                if ( $carosello.length ) {
                    // elimino testo da frecce carosello
                    $caroselloFrecce.find("button").text('');
                    caroselloInitControl($("#slick-slide00"));
                    $caroselloFrecce.css("visibility","visible");
                    //$caroselloDots.css("visibility","visible");
                    $( "ul.slick-dots" ).css("visibility","visible");
                    // colore carosello blocchetto soltanto su mobile
                    $( ".view-carosello .blocchetto[data-colsfo]" ).each(function(i){
                        var myColoreSfondo = $(this).attr("data-colsfo");
                        $(this).find( "h2" ).css("background-color",myColoreSfondo);
                        $(this).find( "p" ).css("background-color",myColoreSfondo);
                        $(this).find( "div.cta a" ).css("background-color",myColoreSfondo);
                        var myColoreTesto = $(this).attr("data-coltex");
                        $(this).find( "h2" ).css("color",myColoreTesto);
                        $(this).find( "p" ).css("color",myColoreTesto);
                        $(this).find( "div.cta a" ).css("color",myColoreTesto);
                        $(this).find( "div.cta a" ).css("border-color",myColoreTesto);
                        $(this).find( "div.cta a" ).mouseover(function() {
                            $(this).css("border-color",myColoreSfondo);
                        }).mouseout(function() {
                            $(this).css("border-color",myColoreTesto);
                        });
                    });
                }
            })
            
            // esegue quando cambiano le dimensioni della finestra
            $(window).resize(function(){
                menuInit();
                if ($menuOrizzontale.length) {
                    menuOrizzontalePosiziona();
                };            
                headerRegionPos = $headerRegion.offset().top;
                hamburgerMenuHide();
            })
            // esegue quando si scorre
            $(window).scroll(function() {
                var scrollPos = $(document).scrollTop();
                //con---sole.log(scrollPos);
                if (scrollPos >= headerRegionPos) {
                    $headerRegion.addClass('sticky');
                    $headerRegion.children().addClass('sticky');
                    $headerRegionLogoWrapper.addClass('sticky');
                    $menuBackground.addClass('sticky');
                } else {
                    $headerRegion.removeClass('sticky');
                    $headerRegion.children().removeClass('sticky');
                    $headerRegionLogoWrapper.removeClass('sticky');
                    $menuBackground.removeClass('sticky');
                }
            });  
            // apertura menu principale da voci            
            // usare once altrimenti quando loggati dentro esegue diverse volte lo script per ogni click, col risultato che si apre e si chiude nello stesso momento
            //
            //
            // VEDI ANCHE jquery.once su
            // https://www.drupal.org/docs/8/api/javascript-api/javascript-api-overview
            // https://www.drupal.org/project/drupal/issues/2978900
            //////$menuLiv0Voce.children(".voceLiv0").once().click(function() {
            //////    event.preventDefault(); // impedisce che # faccia tornare in alto nella pagina
            //////    if ($('body').hasClass("menuActive") && $(this).parent().hasClass( activeClass )) {
            //////        menuHide(activeClass);
            //////    } else {
            //////        var activeMenuClasses = $(this).parent().attr('class').split(" ") ;
            //////        var activeMenuClass =  "." + activeMenuClasses[1];
            //////        //con---sole.log(activeMenuClass);
            //////        $activeMenu = $menuLiv0Menu.find( activeMenuClass );
            //////        menuActive($activeMenu,true,activeClass,true);
            //////    };
            //////});            
            ////// proviamo a sostituire click() con on()
            ////// da https://stackoverflow.com/a/29238631/8585069
            ////// e anche https://stackoverflow.com/questions/14205769/alternative-to-jquery-live-that-can-work#comment19698455_14205769

            $menuLiv0Voce.children(".voceLiv0").once("clicco-menu-once").click(function() {
                event.preventDefault(); // impedisce che # faccia tornare in alto nella pagina
                if ($('body').hasClass("menuActive") && $(this).parent().hasClass( activeClass )) {
                    menuHide(activeClass);
                } else {
                    var activeMenuClasses = $(this).parent().attr('class').split(" ") ;
                    var activeMenuClass =  "." + activeMenuClasses[1];
                    //con---sole.log(activeMenuClass);
                    $activeMenu = $menuLiv0Menu.find( activeMenuClass );
                    menuActive($activeMenu,true,activeClass,true);
                };
            });
            
            //wot
            //$menuBackground.once("clicco-fuori-da-menu").click            
            $(document).once("clicco-qualsiasi-click").click(function(e) {
                var myPadreCN = e.target.parentElement.className;
                var myTargetCN = e.target.className;
////                if ( myTargetCN.indexOf("region") === -1 ) {
////                    var myAntenatoCN = $(this).parents(".region").className;
////                } else {
////                    var myAntenatoCN = myTargetCN;
////                }
////                //myAntenatoCN.css("border","1px solid red");
//                console.log("myTargetCN:    "+myTargetCN);
//                console.log("myPadreCN:     "+myPadreCN);
//                //console.log("myAntenatoCN:  "+myAntenatoCN);
//                e.preventDefault();
                if ( myPadreCN.indexOf("voceLiv0") === -1 ) {
//                    console.log("menuHide");
                    menuHide(activeClass);
                }
                //else if ( myPadreCN.indexOf("voceLiv0") >= 0 ) {
                //    // abbiamo cliccato le prime voci del menu
                //}
            });
    
            // smooth anchor scroll
            // da https://stackoverflow.com/a/7717572/8585069
// DA FARE
            $('a[href^="#"]').once("clicco-anchor-link-smooth").click(function() {
                var href = $.attr(this, 'href');
                $root.animate({
                    //scrollTop: $(href).offset().top
                    scrollTop: $('[name="' + $.attr(this, 'href').substr(1) + '"]').offset().top
                }, 500, function () {
                    window.location.hash = href;
                });
                return false;
            });
            
            // apertura menu search
            $searchLente.once( "clicco-search-once" ).click(function() {
                if ( $searchWrapper.hasClass( "active" ) ) {
                    searchHide();
                } else {
                    menuHide( activeClassNoTransition );
                    searchActive();
                };
            });

            //colora banner menu sx
            var $menuViewsRow = $(".view-menu-2-aside-sx .views-row");
            $menuViewsRow.each(function(i){
                var myColore = $(this).children(".colorePers").attr("data");
                $(this).find( ".etichetta" ).css("color",myColore);
                $(this).find( ".blocchetto" ).css("border-color",myColore);
                $(this).find( "a" ).css("text-decoration","none !important");
                $(this).find( "a" ).css("text-decoration-color",myColore);
            });
            $menuViewsRow.mouseover(function(){
                var myColore = $(this).children(".colorePers").attr("data");
                var myColoreBackground = "rgba(" + hex2rgb( $(this).children(".colorePers").attr("data") ) + ",0.1)";
                $(this).find( ".blocchetto" ).css("background-color",myColoreBackground);
                $(this).find( ".testo" ).css("color",myColore);
            });
            $menuViewsRow.mouseout(function(){
                $(this).find( ".blocchetto" ).css("background-color","transparent");
                $(this).find( ".testo" ).css("color","#0a0505");
            });
            
            //banner corsi al passaggio del mouse
            if ( $bannerCorsiView.length ) {
                $bannerCorsiVoce.mouseover(function(){
                    myGif = $(this).attr("data");
                    myPrevImg = $(this).find(".immagine").attr("data");
                    $(this).find("img").attr("src",myGif);
                });
                $bannerCorsiVoce.mouseleave(function(){
                    myPrevImg = $(this).find(".immagine").attr("data");
                    $(this).find("img").attr("src",myPrevImg);
                });
            };

            // da ottimizzare
            // carosello. lancia calcolo posizione arrow e dot quando cambia slide (vedi anche carosello. Create a closure)
            $(function(){
                $("#slick-slide00").bind('cssClassChanged', function(){
                    var myDom = $(this);
                    caroselloInitControl(myDom);
                });
                $("#slick-slide01").bind('cssClassChanged', function(){
                    var myDom = $(this);
                    caroselloInitControl(myDom);
                });
                $("#slick-slide02").bind('cssClassChanged', function(){
                    var myDom = $(this);
                    caroselloInitControl(myDom);
                });
                $("#slick-slide03").bind('cssClassChanged', function(){
                    var myDom = $(this);
                    caroselloInitControl(myDom);
                });
                $("#slick-slide04").bind('cssClassChanged', function(){
                    var myDom = $(this);
                    caroselloInitControl(myDom);
                });
            });
            
            
            //menu mobile
            // $hamburger.on("click", function(e) {
            // fixme con loggati dentro non funziona
            $hamburger.once("clicco-hamburger-once").click(function() {
                $hamburger.toggleClass("is-active");
                // Do something else, like open/close menu
                hamburgerMenuActive();
                searchHide();
            });
            //accordion menu cellulare
            // qui funziona anche senza once()
            $(".menuPrincipaleMobile .menuLiv0Voce .voceLiv0").click(function(){
                if ($(this).hasClass("voceLiv0")) {
                    //con---sole.log("hei");
                    event.preventDefault();
                    if ($(this).parent( ".menuLiv0Voce" ).children( ".sottoMenu" ).hasClass("active")) {
                        $( ".menuPrincipaleMobile" ).find( ".sottoMenu" ).removeClass("active");
                    } else {
                        $( ".menuPrincipaleMobile" ).find( ".sottoMenu" ).removeClass("active");
                        $(this).parent( ".menuLiv0Voce" ).children( ".sottoMenu" ).addClass( "active" );
                    }
                }
            });
            
            //disabilito il clic sulle prime voci del menu footer
            //$(".menuPrincipaleMobile .menuLiv0Voce .voceLiv0").click(function(){
            $primeVociMenuFooter.once("clicco-primevocimenufooter-once").click(function() {
                event.preventDefault(); // impedisce che # faccia tornare in alto nella pagina                
            });
            
            // fine del codice personalizzato !!
            // fine del codice personalizzato !!
            // fine del codice personalizzato !!
        }
    };
})(jQuery, Drupal);
// da https://stackoverflow.com/a/14084869/8585069
// carosello. Create a closure
(function(){
    // Your base, I'm in it!
    var originalAddClassMethod = jQuery.fn.addClass;

    jQuery.fn.addClass = function(){
        // Execute the original method.
        var result = originalAddClassMethod.apply( this, arguments );

        // trigger a custom event
        jQuery(this).trigger('cssClassChanged');
        //con---sole.log("hei trigger");

        // return the original result
        return result;
    }
})();
